Categories:Writing
License:GPL-3.0
Web Site:https://github.com/YoeriNijs/NoteBuddy/blob/HEAD/README.md
Source Code:https://github.com/YoeriNijs/NoteBuddy
Issue Tracker:https://github.com/YoeriNijs/NoteBuddy/issues

Auto Name:NoteBuddy
Summary:Store encrypted notes
Description:
Notes are encrypted using the AES Crypto library by Tozny LLC. Key generation,
encryption, and decryption are using 128-bit AES, CBC, PKCS5 padding, and a
random 16-byte IV with SHA1PRNG.

To use NoteBuddy, one must setup a simple user account (username and numerical
password) in order to login. Moreover, one may create a secret question in order
to gain access when the password is lost. All settings are stored encrypted in
Android's Shared Preferences for now. The idea is to work with a SQlite database
on the long term.
.

Repo Type:git
Repo:https://github.com/YoeriNijs/NoteBuddy

Build:1.0,1
    commit=e7c896ba1a57efaf5a8d1d1019c16c54b467aceb
    subdir=app
    gradle=yes

Build:1.0.1,2
    commit=6f21af2761116ea02cbc324d6b4db0d2d89f529e
    subdir=app
    gradle=yes

Build:1.1.1,4
    commit=c6080b9bacb6a8eb2804f0408b49e98a065f938b
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.1
Current Version Code:4
