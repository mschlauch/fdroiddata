Categories:Multimedia
License:GPL-3.0
Web Site:https://gitlab.com/axet/android-hourly-reminder
Source Code:https://gitlab.com/axet/android-hourly-reminder/tree/HEAD
Issue Tracker:https://gitlab.com/axet/android-hourly-reminder/issues

Auto Name:Hourly Reminder
Summary:Remind you about hours passed
Description:
Never miss an alarm: This app reminds you about hours passed. Application will
pronounce current time on user specified hours. You can choice alarm types:
beep, speech or custom wav/ogg files.
.

Repo Type:git
Repo:https://gitlab.com/axet/android-hourly-reminder.git

Build:2.2.15,239
    commit=hourlyreminder-2.2.15
    subdir=app
    gradle=yes

Auto Update Mode:Version hourlyreminder-%v
Update Check Mode:Tags
Current Version:2.2.15
Current Version Code:239
